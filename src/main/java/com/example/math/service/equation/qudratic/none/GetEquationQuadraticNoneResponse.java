package com.example.math.service.equation.qudratic.none;

import com.example.math.entity.Solution;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetEquationQuadraticNoneResponse {
    private Solution solution;
}
